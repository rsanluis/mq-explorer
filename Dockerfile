FROM centos 

# Modified from https://github.com/ibm-messaging/mq-container/tree/master/incubating/mq-explorer 

# Tested on: 
#   Mac OS: v10.15.3 (Catalina)
#   Docker: 2.2.0.3 (42716)
#   XQuartz: v2.7.11 (xorg-server 1.18.4) 


ARG MQ_URL="https://public.dhe.ibm.com/ibmdl/export/pub/software/websphere/messaging/mqadv/mqadv_dev912_linux_x86-64.tar.gz"
ENV MQ_PACKAGES="MQSeriesRuntime*.rpm MQSeriesJRE*.rpm MQSeriesExplorer*.rpm"
ARG MQM_UID=888
RUN yum -y install which gtk3 xauth mesa-libGL
ADD install-mq.sh /usr/local/bin/

RUN chmod u+x /usr/local/bin/install-mq.sh \
  && sleep 1 \
  && install-mq.sh $MQM_UID \
  && rm -rf /var/mqm \
  && /opt/mqm/bin/crtmqdir -f -s

ENV LANG=en_US.UTF-8
ENV SWT_GTK3=1
USER $MQM_UID
ENTRYPOINT ["MQExplorer"]
